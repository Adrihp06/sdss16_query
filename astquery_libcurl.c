#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
 
struct MemoryStruct {
  char *memory;
  size_t size;
};
 
static size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  struct MemoryStruct *mem = (struct MemoryStruct *)userp;
 
  char *ptr = realloc(mem->memory, mem->size + realsize + 1);
  if(ptr == NULL) {
    /* out of memory! */ 
    printf("not enough memory (realloc returned NULL)\n");
    return 0;
  }
 
  mem->memory = ptr;
  memcpy(&(mem->memory[mem->size]), contents, realsize);
  mem->size += realsize;
  mem->memory[mem->size] = 0;
 
  return realsize;
}

int main(int argc, char *argv[])
{
  CURLcode ret;
  CURL *hnd;
  curl_mime *mime1;
  curl_mimepart *part1;
  struct MemoryStruct query;
 
  query.memory = malloc(1);  
  query.size = 0;     

  mime1 = NULL;

  hnd = curl_easy_init();
  curl_easy_setopt(hnd, CURLOPT_BUFFERSIZE, 102400L);
  curl_easy_setopt(hnd, CURLOPT_URL, "https://gea.esac.esa.int/tap-server/tap/sync");
  curl_easy_setopt(hnd, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
  curl_easy_setopt(hnd, CURLOPT_WRITEDATA, (void *)&query);
  curl_easy_setopt(hnd, CURLOPT_NOPROGRESS, 1L);
  mime1 = curl_mime_init(hnd);
  part1 = curl_mime_addpart(mime1);
  curl_mime_data(part1, "ADQL", CURL_ZERO_TERMINATED);
  curl_mime_name(part1, "LANG");
  part1 = curl_mime_addpart(mime1);
  curl_mime_data(part1, "csv", CURL_ZERO_TERMINATED);
  curl_mime_name(part1, "FORMAT");
  part1 = curl_mime_addpart(mime1);
  curl_mime_data(part1, "doQuery", CURL_ZERO_TERMINATED);
  curl_mime_name(part1, "REQUEST");
  part1 = curl_mime_addpart(mime1);
  curl_mime_data(part1, "SELECT  source_id,ra,dec FROM gaiaedr3.gaia_source WHERE   1=CONTAINS( POINT('ICRS', ra, dec), CIRCLE('ICRS', 113.87297610, 31.90271520, 0.0166667) )", CURL_ZERO_TERMINATED);
  curl_mime_name(part1, "QUERY");
  curl_easy_setopt(hnd, CURLOPT_MIMEPOST, mime1);
  curl_easy_setopt(hnd, CURLOPT_USERAGENT, "curl/7.75.0");
  curl_easy_setopt(hnd, CURLOPT_MAXREDIRS, 50L);
  curl_easy_setopt(hnd, CURLOPT_FTP_SKIP_PASV_IP, 1L);
  curl_easy_setopt(hnd, CURLOPT_TCP_KEEPALIVE, 1L);


  ret = curl_easy_perform(hnd);
  printf("%s", query.memory);

  curl_easy_cleanup(hnd);
  hnd = NULL;
  curl_mime_free(mime1);
  mime1 = NULL;

  return (int)ret;
}

